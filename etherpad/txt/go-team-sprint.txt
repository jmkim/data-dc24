Go Team Sprint

 Daily Location (reflects where people are currently working)
	* August 4 -- Ad-hoc

Participants
	*  sourabhtk37
	* tille (just want to discuss some general team issues no technical Go details)
	* gibmat
	* omnidapps (Floating Resource)
	* siretart (remote?)

Remaining work as of breakfast, Sunday, Aug 4
	* Upload of etcd 3.5.15-5
		* Should fix build rdep FTBFS issues
		* Hopefully autopkgtests will all pass; if not, we'll (for now) skip those tests
	* After etcd is happy (ie, all autopkgtests passing), upload golang-google-grpc 1.64.0-6
		* Adds one more Breaks line, which should then get all of its autopkgtests happy
	* Fix containerd autopkgtest failures
		* https://bugs.debian.org/1077319
	* Fix remaining FTBFS packages:
		* golang-github-backblaze-blazer
			* still builds against grpc-gateway.v1, might be the last package to do so
			* upstream does not appear to be accepting bug reports at https://github.com/Backblaze/blazer/issues/new
				* this is what the package currently claims: https://sources.debian.org/src/golang-github-backblaze-blazer/0.6.1-2/debian/upstream/metadata/
			* unclear how to resolve this. maybe vendoring old sources, or removing from the archive.
			* Filed https://bugs.debian.org/1077924 as RC bug
			* Turns out this is a dependency of 'restic', a pretty popular backup utility
			* turns out that we can avoid the grpc dependency by excluding the 'bonfire' and 'pyre' packages. Apparently they are used for the b2 service itself that is not used by restic
	* Fix remaining packages with build conflicts:
		* mirrorbits (https://bugs.debian.org/1077366) (NMU'ed)
		* google-guest-agent (https://bugs.debian.org/1077779) 
			* (NMU uploaded to DELAYED/5-days, -- 08/03)
		* golang-github-hashicorp-go-plugin (uploaded)
		* nextcloud-spreed-signaling
			* Update ready in salsa, pending new upload of etcd first
	* wasmedge (libpod dependency) has an RC bug; is on the LowNMU list
		* Looking at the bug, this is an FTBFS introduced by GCC-14
		* Upstream code is newer than what we have, but does not contain any changes to the relevant part of the code: https://github.com/WasmEdge/WasmEdge/blob/0.13.5/lib/loader/filemgr.cpp#L52
		* This is very likely still present upstream, but I don't see an issue filed
		* For Debian, I'd suggest to remove the -Werror from here: https://sources.debian.org/src/wasmedge/0.13.5+dfsg-1/cmake/Helper.cmake/?hl=42#L42 -- and have a bug filed upstream to discuss this issue
		* NMU uploaded
	* After migration of all packages into testing:
		* Go back and clean up golang-google-grpc's exclusion of source files that isn't currently working correctly
		* File RM bug for golang-github-grpc-ecosystem-grpc-gateway.v2
		* Fix dependency loop seen while building etcd (it pulls in its own -dev package via some transitive dependency path)
		* Cleanup etcd's lintian report
		* Fixup/address etcd's flaky autopkgtests
			* should be fixed in https://salsa.debian.org/go-team/packages/etcd/-/commit/34a35c19e805152c2cb22b65fff7899d68b2815d and https://salsa.debian.org/go-team/packages/etcd/-/commit/518af100c1b5ec3b19dd710c9a668eb2e5f9febf -- will be part of -7










Old information below












Important/blocking issues for transition:
	* golang-github-grpc-ecosystem-grpc-gateway FTBFS on 32bit (#1077062)
	* autopkgtest failures for golang-github-google-cel-go
	* golang-google-grpc including source files despite DH_GOLANG_EXCLUDES
		* For whatever reason, even though during the build it doesn't appear to build/test targets listed in d/rules, the resulting -dev package still contains them
		* This was causing an autopkgtest failure, but gibmat did some brute force work to make it pass again in version 1.64.0-3; it really needs a better investigation when this package isn't blocking everything else
		* Oddly enough, removing the import path from DH_GOLANG_EXCLUDES gets things working without any further workarounds. Seemingly, install does not seem to honor import path related excludes in this package. -> Fix pushed to salsa.
	* etcd from experimental FTBFS due to changes in grpc-gateway API
		* Plan is to update to latest 3.5 release of etcd
			* Initial import of 3.5.15 and rebasing of patches pushed to salsa
			* Need to setup compat symlinks for various import paths (see go.mod)
		* Build dependencies are now in unstable, but still some protobuf-API related issues to fix
		* siretart is making progress on getting it built against grpc-gateway.v2 -- candidate package in salsa -- uploaded to sid
	* etcd FTBFS due to golang-github-grpc-ecosystem-go-grpc-middleware v2 update
	* etcd autopkgtest failure
		* Also need to fix installing of compat symlinks
	* golang-github-backblaze-blazer FTBFS due to changes in grpc-gateway API
		* Attempting to re-generate runs into issues with the googleapis proto no longer being in gateway.v2
		* Easy enough to add them back in, but gibmat thinks he doesn't have the blazer codegen setup properly (WIP)
	* golang-github-containerd-imgcrypt FTBFS due to updated containerd
		* siretart is working on it. fixed upstream in https://github.com/containerd/imgcrypt/commit/27550399fb8c51b4987f9f95c5c99f88e8011643
		* fixed package uploaded to sid as 1.1.11-4
	* crowdsec FTBFS due to updated docker
		* Fixed, patch sent to ML
	* containerd autopkgtests are failing and need investigation
		* siretart looking at this. cf. https://bugs.debian.org/1077319
	* golang-github-census-instrumentation-opencensus-proto has conflicting build deps
	* golang-github-go-kit-kit has conflicting build deps
	* golang-github-grpc-ecosystem-go-grpc-middleware has conflicting build deps
		* siretart looking at packaging the latest upstream version -- pushed and uploaded 2.1.0-1
	* golang-github-openzipkin-zipkin-go has conflicting build deps
	* golang-go.opencensus has conflicting build deps
	* mirrorbits has conflicting build deps
		* filed  https://bugs.debian.org/1077366 -- attached NMU.diff
	* golang-github-expediadotcom-haystack-client-go has conflicting build deps
	* golang-github-grafana-grafana-plugin-model has conflicting build deps
	* golang-github-grpc-ecosystem-go-grpc-prometheus has conflicting build deps
	* golang-github-opentracing-contrib-go-grpc has conflicting build deps
	* golang-github-sercand-kuberesolver FTBFS
	* golang-collectd has conflicting build deps
	* golang-gitlab-gitlab-org-labkit FTBFS
	* docker-libkv FTBFS after etcd update
	* golang-github-minio-pkg FTBFS after etcd update
	* golang-github-xordataexchange-crypt FTBFS after etcd update
	* Additional packages with conflicting build deps:
		* caddy & golang-github-smallstep-certificates
		* golang-github-hashicorp-go-discover
		* golang-github-hashicorp-go-plugin
		* google-guest-agent
			* #1077779
		* nextcloud-spreed-signaling
			* Tracked down to golang-github-nats-io-go-nats
			* Needs fix/adjustment to etcd packaging before upload
		* opensnitch
		* protobuild (siretart working on it)

Other things to work on:
	* wasmedge (libpod dependency) has an RC bug; is on the LowNMU list
		* Looking at the bug, this is an FTBFS introduced by GCC-14
		* Upstream code is newer than what we have, but does not contain any changes to the relevant part of the code: https://github.com/WasmEdge/WasmEdge/blob/0.13.5/lib/loader/filemgr.cpp#L52
		* This is very likely still present upstream, but I don't see an issue filed
		* For Debian, I'd suggest to remove the -Werror from here: https://sources.debian.org/src/wasmedge/0.13.5+dfsg-1/cmake/Helper.cmake/?hl=42#L42 -- and have a bug filed upstream to discuss this issue


	* Email thread: https://lists.debian.org/debian-go/2024/07/msg00021.html
		* ACK'ed from Release Team 7/25: https://lists.debian.org/debian-go/2024/07/msg00052.html
	* Suggested order of uploads from experimental to unstable:
		0.1. golang-google-genproto
		0.2. golang-google-grpc
		0.3. golang-github-grpc-ecosystem-grpc-gateway.v2
			0.3.1. Instead we will update golang-github-grpc-ecosystem-grpc-gateway to v2
			0.3.2. NB: this breaks etcd, golang-github-backblaze-blazer
		0.4. golang-github-google-cel-go
		0.5. golang-opentelemetry-proto
		0.6. golang-opentelemetry-otel
		0.7. golang-opentelemetry-contrib
			0.7.1. Nothing currently staged in experimental -- needed as part of this transition?
		0.8. golang-github-googleapis-gnostic
		0.9. etcd
			0.9.1. Build error due to grpc-gateway API, need update to 3.5
		0.10. golang-gogottrpc
		0.11. golang-github-containerd-log
		0.12. golang-github-containerd-btrfs
		0.13. golang-github-containerd-go-cni
		0.14. golang-github-containerd-go-runc
		0.15. golang-github-coreos-bbolt
		0.16. golang-github-google-certificate-transparency
		0.17. golang-github-lightstep-lightstep-tracer-common
		0.18. golang-github-kurin-blazer
			0.18.1. RM'ed in favor of golang-github-backblaze-blazer
			0.18.2. See notes above in important/blocking issues section
		0.19. golang-google-cloud
		0.20. golang-github-containerd-cgroups
		0.21. golang-github-containerd-typeurl
		0.22. containerd
		0.23. golang-github-cloudflare-cfssl
		0.24. rootlesskit
		0.25. golang-github-deckarep-golang-set
		0.26. notary
		0.27. golang-github-mitchellh-hashstructure
		0.28. docker.io
		0.29. golang-github-fsouza-go-dockerclient
		0.30. golang-github-openshift-imagebuilder
		0.31. golang-github-containers-storage
		0.32. golang-github-containers-image
		0.33. golang-github-containers-common
		0.34. golang-github-containers-buildah
		0.35. libpod
