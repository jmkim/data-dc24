Workshop link: https://docs.lkcamp.dev/unicamp_group/workshop/

	
# READ THIS AFTER EXECUTING CHECKPATCH

* Try to find a coding style to fix that you don't need to compile to check if it is ok
* Try to find something that doesn't change the logic
* Do not mix different types of modifications in one patch.
* Check if in the folder driver there isn't a README, a notes.txt our a TODO file that says this driver is deprecated and it will be dropped

# READ THIS BEFORE SUBMITTING YOUR PATCH

* Write a comment after "---" in the patch explaining this is your first contribution
* CC the patch to helen.koike@collabora.com as well


=========================

WRITE HERE YOUR NAME AND WHICH ERROR YOU ARE FIXING (so we don't send duplicated patches)

WARNING: function definition argument 'int' should also have an identifier name -> gagath
#1268: FILE: drivers/staging/vme_user/vme.c:1268:
    https://lore.kernel.org/linux-staging/20240730062843.64977-1-gagath@debian.org/T/#u

ERROR: space prohibited before that close parenthesis ')'  -> sergiosacj
#65: FILE: fbtft/fbtft-bus.c:65:

WARNING: macros should not use a trailing semicolon -> saki
#356: FILE: fbtft/fbtft.h:356:

CHECK: Alignment should match open parenthesis -> Thais-ra
#95: FILE: drivers/staging/greybus/gb-camera.h:95:

ERROR: code indent should use tabs where possible -> eipifun
#78: FILE: drivers/staging/rtl8723bs/include/rtw_security.h:78:

ERROR: that open brace { should be on the previous line  -> puidan
#173: FILE: drivers/staging/rtl8723bs/include/rtw_security.h:173:

ERROR: code indent should use tabs where possible -> santiago
#388: FILE: drivers/staging/rtl8723bs/include/rtw_mlme_ext.h:388:

ERROR: that open brace { should be on the previous line -> sskartheekadivi
#83: FILE: drivers/staging/rtl8723bs/include/osdep_service.h:83:

ERROR: "foo * bar" should be "foo *bar" -> Count-Dracula
#105: FILE: drivers/staging/rtl8723bs/include/osdep_service.h:105:

ERROR: code indent should use tabs where possible -> bensmrs
#40: FILE: drivers/staging/rtl8723bs/include/osdep_service_linux.h:40:

ERROR: code indent should use tabs where possible -> eamanu
#104: FILE: drivers/staging/rtl8723bs/include/hal_pwr_seq.h:104

ERROR: trailing whitespace -> Felix Yan
#739: FILE: drivers/staging/rtl8712/rtl871x_cmd.h:739:

ERROR: Macros with multiple statements should be enclosed in a do - while loop -> zigo
#165: FILE: drivers/staging/greybus/loopback.c:165:

ERROR: trailing statements should be on next line -> sergiosacj
#47: FILE: drivers/staging/media/atomisp/pci/hive_isp_css_include/assert_support.h:47:

ERROR: trailing statements should be on next line ->Count-Dracula
#48: FILE: drivers/staging/media/atomisp/pci/isp/kernels/bnr/bnr_1.0/ia_css_bnr.host.c:48:

CHECK: Lines should not end with a '('
#55: FILE: drivers/staging/media/atomisp/pci/isp/kernels/anr/anr_1.0/ia_css_anr.host.c:55:

ERROR: open brace '{' following function definitions go on the next line -> santiago
#239: FILE: drivers/staging/media/atomisp/pci/isp/kernels/sdis/sdis_1.0/ia_css_sdis.host.c:239:

ERROR: that open brace { should be on the previous line -> santiago
#253: FILE: drivers/staging/media/atomisp/pci/isp/kernels/sdis/sdis_1.0/ia_css_sdis.host.c:253:
+    if (map)
+    {
--
ERROR: that open brace { should be on the previous line -> santiago
#258: FILE: drivers/staging/media/atomisp/pci/isp/kernels/sdis/sdis_1.0/ia_css_sdis.host.c:258:
+    } else
+    {

ERROR: Macros with complex values should be enclosed in parentheses -> spwhitton
#41: FILE: drivers/staging/media/atomisp/pci/hive_isp_css_include/sp.h:41:

ERROR: trailing statements should be on next line
#48: FILE: drivers/staging/media/atomisp/pci/isp/kernels/anr/anr_1.0/ia_css_anr.host.c:48: -> sskartheekadivi

ERROR: trailing statements should be on next line -saki
#46: FILE: drivers/staging/media/atomisp/pci/isp/kernels/fpn/fpn_1.0/ia_css_fpn.host.c:46:

ERROR: trailing statements should be on next line
#62: FILE: drivers/staging/media/atomisp/pci/isp/kernels/wb/wb_1.0/ia_css_wb.host.c:62:

CHECK: Lines should not end with a '('
#77: FILE: drivers/staging/media/atomisp/pci/isp/kernels/wb/wb_1.0/ia_css_wb.host.c:77:
i
ERROR: trailing statements should be on next line -> kathara
#50: FILE: drivers/staging/media/atomisp/pci/isp/kernels/de/de_1.0/ia_css_de.host.c:50:

ERROR: open brace '{' following function definitions go on the next line -> milan
#33: FILE: drivers/staging/media/atomisp/pci/isp/kernels/raw/raw_1.0/ia_css_raw.host.c:33:

ERROR: trailing statements should be on next line -> aquilamacedo
#88: FILE: drivers/staging/media/atomisp/pci/isp/kernels/dp/dp_1.0/ia_css_dp.host.c:88:

CHECK: Lines should not end with a '('
#117: FILE: drivers/staging/media/atomisp/pci/isp/kernels/dp/dp_1.0/ia_css_dp.host.c:117:

WARNING: please, no spaces at the start of a line kathara
#118: FILE: drivers/staging/media/atomisp/pci/isp/kernels/dp/dp_1.0/ia_css_dp.host.c:118:

ERROR: trailing statements should be on next line
#41: FILE: drivers/staging/media/atomisp/pci/isp/kernels/sc/sc_1.0/ia_css_sc.host.c:41: -> José

WARNING: Block comments use * on subsequent lines
#49: FILE: drivers/staging/media/atomisp/pci/isp/kernels/sc/sc_1.0/ia_css_sc.host.c:49:

ERROR: trailing statements should be on next line
#139: FILE: drivers/staging/media/atomisp/pci/isp/kernels/output/output_1.0/ia_css_output.host.c:139:

ERROR: trailing statements should be on next line
#125: FILE: drivers/staging/media/atomisp/pci/isp/kernels/ob/ob_1.0/ia_css_ob.host.c:125:

ERROR: trailing statements should be on next line
#73: FILE: drivers/staging/media/atomisp/pci/isp/kernels/csc/csc_1.0/ia_css_csc.host.c:73:

ERROR: that open brace { should be on the previous line -> tchad
#67: FILE: drivers/staging/media/atomisp/pci/isp/kernels/iterator/iterator_1.0/ia_css_iterator.host.c:67:

ERROR: open brace '{' following function definitions go on the next line
#270: FILE: drivers/staging/media/atomisp/pci/isp/kernels/dvs/dvs_1.0/ia_css_dvs.host.c:270:

ERROR: trailing statements should be on next line
#53: FILE: drivers/staging/media/atomisp/pci/isp/kernels/tnr/tnr_1.0/ia_css_tnr.host.c:53:
=

CHECK: Alignment should match open parenthesis
#191: FILE: drivers/staging/rtl8192e/rtl819x_HTProc.c:191: -> taffit
https://lore.kernel.org/linux-staging/20240730064707.914-1-david@tilapin.org/T/#u

Where to go next
===================

To check if your patch arrived in the linux-staging ML: see https://lore.kernel.org/linux-staging/

Write a hello world char driver: https://docs.lkcamp.dev/unicamp_group/dev_drivers/
Do the v4l2 exercises: https://docs.lkcamp.dev/unicamp_group/workshop-v4l/
Do the cripto exercises: https://docs.lkcamp.dev/unicamp_group/crypto_kernel/
